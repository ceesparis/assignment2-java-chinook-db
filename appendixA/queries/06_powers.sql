INSERT INTO superpower (sp_name, sp_description) VALUES ('Clairvoyance', 'can see into future and also the past why not');
INSERT INTO superpower (sp_name, sp_description) VALUES ('Breathing Underwater', 'like any other water dweller');
INSERT INTO superpower (sp_name, sp_description) VALUES ('Laserbeam', 'beaming a laser like it is 1999');
INSERT INTO superpower (sp_name, sp_description) VALUES ('Webbing', 'making spiderwebz etc');

INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (1, 2);
INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (1, 3);
INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (3, 3);
INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (2, 4);
INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (4, 2);
INSERT INTO superhero_superpower (sh_id, sp_id) VALUES (4, 1);

