DROP TABLE IF EXISTS superhero_superpower;

CREATE TABLE superhero_superpower (
	sh_id int REFERENCES superhero,
	sp_id int REFERENCES superpower,
	PRIMARY KEY (sh_id, sp_id)
);

