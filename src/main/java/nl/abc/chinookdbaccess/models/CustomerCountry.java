package nl.abc.chinookdbaccess.models;

public record CustomerCountry (String country) {
}
