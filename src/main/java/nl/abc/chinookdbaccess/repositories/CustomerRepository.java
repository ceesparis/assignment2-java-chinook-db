package nl.abc.chinookdbaccess.repositories;

import nl.abc.chinookdbaccess.models.Customer;
import nl.abc.chinookdbaccess.models.CustomerCountry;
import nl.abc.chinookdbaccess.models.CustomerGenre;
import nl.abc.chinookdbaccess.models.CustomerSpender;

import java.util.ArrayList;


public interface CustomerRepository extends CRUDRepository<Customer, Integer>{


    default void test(){

        // testing req 1
        ArrayList<Customer> customers = this.findAll();
        for (Customer customer : customers) {
            System.out.println(customer);
        }

        // testing req 2
        Customer firstCustomer = this.findById(1);
        System.out.println(firstCustomer);

        // testing req 3
        Customer customerByName = this.findByName("Diego");
        System.out.println(customerByName);

        // testing req 4
        ArrayList<Customer> customers10to30 = this.findSubset(20, 10);
        System.out.println(customers10to30);

        // testing req 5
        int rows_affected = this.insert(new Customer(-1, "Dewald", "Els", "South Africa", "5241", "+1234567890", "dewald.els@noroff.no"));
        System.out.println(rows_affected);

        // testing req 6
        Customer dewald = this.findByName("Dewald");
        Customer nicholas = new Customer(
                dewald.id(),
                "Nicholas",
                "Lennox",
                dewald.country(),
                dewald.postalCode(),
                dewald.phone(),
                "nicholas.lennox@noroff.no"
        );

        int updatedRows = this.update(nicholas);
        System.out.println("Updated rows: " + updatedRows);

        // testing req 7
        System.out.println(findCountryWithMostCustomers());

        // testing req 8
        System.out.println(findHighestSpender());

        // testing req 9
        System.out.println(findMostPopularGenreCustomer(12));
    }

    /**
     * Method that finds a customer by first or last name.
     *
     * @param name {@link String}
     *
     * @return customer {@link Customer}
     */
    Customer findByName(String name);

    /**
     * Method that returns a subset of the customers in the database.
     *
     * @param offset {@link int} starting point of the subset
     * @param limit {@link int} total length of the subset
     *
     * @return customerList {@link ArrayList<Customer>}
     */
    ArrayList<Customer> findSubset(int offset, int limit);


    /**
     * Method that returns the country with most customers.
     *
     * @return country {@link CustomerCountry}
     */
    CustomerCountry findCountryWithMostCustomers();

    /**
     * Method that returns the customer who has spent the highest amount in total.
     *
     * @return A single {@link CustomerSpender} record
     */
    CustomerSpender findHighestSpender();

    /**
     * Method that returns the most popular track genre(s) of a customer.
     *
     * @param id The id of the customer to check for
     * @return The most occurring genre(s) of tracks this customer has purchased
     */
    ArrayList<CustomerGenre> findMostPopularGenreCustomer(int id);
}
