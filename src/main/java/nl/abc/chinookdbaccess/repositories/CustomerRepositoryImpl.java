package nl.abc.chinookdbaccess.repositories;

import nl.abc.chinookdbaccess.models.Customer;
import nl.abc.chinookdbaccess.models.CustomerCountry;
import nl.abc.chinookdbaccess.models.CustomerGenre;
import nl.abc.chinookdbaccess.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl (
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Functional interface used to modify a {@link ResultSet}
     */
    @FunctionalInterface
    private interface ResultSetHandler {
        /**
         * Handles a ResultSet
         * @param result The ResultSet of an executed query
         * @exception SQLException In case an SQL error occurs
         */
        void handleResultSet(final ResultSet result) throws SQLException;
    }

    /**
     * Functional interface used to modify a {@link PreparedStatement}
     */
    @FunctionalInterface
    private interface StatementHandler {
        /**
         * Modifies a prepared statement
         * @param preparedStatement The statement to modify before executing it
         * @exception SQLException In case an SQL error occurs
         */
        void modifyStatement(final PreparedStatement preparedStatement) throws SQLException;
    }

    /**
     * Helper method that connects to the database and executes an SQL query with a prepared statement.
     *
     * @param sql The SQL query to execute
     * @param statementHandler Functional interface used to modify the prepared statement
     * @param resultSetHandler Functional interface used to handle the set of results
     */
    private void ExecuteQuery(String sql, StatementHandler statementHandler, ResultSetHandler resultSetHandler) {
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            statementHandler.modifyStatement(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSetHandler.handleResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Overloaded method for executing an SQL query without a prepared statement.
     *
     * @param sql The SQL query to execute
     * @param resultSetHandler Functional interface used to handle the set of results
     */
    private void ExecuteQuery(String sql, ResultSetHandler resultSetHandler) {
        ExecuteQuery(sql, preparedStatement -> {}, resultSetHandler);
    }

    /**
     * Helper method for connecting to the database and executing a statement that performs an update.
     *
     * @param sql The SQL statement to execute
     * @param statementHandler Functional interface used to modify the prepared statement
     *
     * @return The number of rows affected by the update
     */
    private int ExecuteUpdate(String sql, StatementHandler statementHandler) {
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            statementHandler.modifyStatement(preparedStatement);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Overloaded method for executing an update statement.
     *
     * @param sql The SQL statement to execute
     *
     * @return The number of rows affected by the update
     */
    private int ExecuteUpdate(String sql) {
        return ExecuteUpdate(sql, preparedStatement -> {});
    }

    /**
     * Helper method that creates a {@link Customer} from a {@link ResultSet}.
     *
     * @param resultSet The {@link ResultSet} containing the customers' fields
     *
     * @return A single {@link Customer} record
     * @exception SQLException In case of an SQL error
     */
    private Customer readCustomer(ResultSet resultSet) throws SQLException {
        return new Customer(
            resultSet.getInt("customer_id"),
            resultSet.getString("first_name"),
            resultSet.getString("last_name"),
            resultSet.getString("country"),
            resultSet.getString("postal_code"),
            resultSet.getString("phone"),
            resultSet.getString("email")
        );
    }

    /**
     * Helper method to set the parameters of a Customer record, excluding its id.
     *
     * @param customer The {@link Customer} record to set the parameters for
     * @param preparedStatement The {@link PreparedStatement} to modify
     * @exception SQLException In case of an SQL error
     */
    private void setCustomerParameters(Customer customer, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, customer.firstName());
        preparedStatement.setString(2, customer.lastName());
        preparedStatement.setString(3, customer.country());
        preparedStatement.setString(4, customer.postalCode());
        preparedStatement.setString(5, customer.phone());
        preparedStatement.setString(6, customer.email());
    }


    @Override
    public ArrayList<Customer> findAll() {
        final ArrayList<Customer> customerList = new ArrayList<>();
        String sql = """
                SELECT customer_id, first_name, last_name, country,
                postal_code, phone, email FROM customer""";

        ExecuteQuery(sql, result -> {
            while (result.next()){
                customerList.add(readCustomer(result));
            }
        });
        return customerList;
    }

    @Override
    public Customer findById(Integer id) {
        final Customer[] customer = new Customer[1];
        String sql = """
                SELECT customer_id, first_name, last_name, country,
                postal_code, phone, email FROM customer
                WHERE customer_id = ?""";

        ExecuteQuery(sql, preparedStatement -> preparedStatement.setInt(1, id), result -> {
            if (result.next()){
                customer[0] = readCustomer(result);
            }
        });
        return customer[0];
    }

    @Override
    public int insert(Customer customer) {
        String sql = """
                INSERT INTO customer
                (first_name, last_name, country, postal_code, phone, email)
                VALUES (?, ?, ?, ?, ?, ?)""";

        return ExecuteUpdate(sql, preparedStatement -> setCustomerParameters(customer, preparedStatement));
    }

    @Override
    public int update(Customer customer) {
        String sql = """
                UPDATE customer
                SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ?
                WHERE customer_id = ?""";

        return ExecuteUpdate(sql, preparedStatement -> {
            setCustomerParameters(customer, preparedStatement);
            preparedStatement.setInt(7, customer.id());
        });
    }

    @Override
    public int delete(Customer object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int deleteById(Integer id) {
        throw new UnsupportedOperationException();
    }


    @Override
    public Customer findByName(String name) {
        final Customer[] customer = new Customer[1];
        String sql = """
                SELECT customer_id, first_name,
                last_name, country, postal_code,
                phone, email FROM customer
                where first_name LIKE ? OR last_name LIKE ?""";

        ExecuteQuery(sql, preparedStatement -> {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, name);
        }, result -> {
            if (result.next()){
                customer[0] = readCustomer(result);
            }
        });
        return customer[0];
    }


    @Override
    public ArrayList<Customer> findSubset(int offset, int limit) {
        final ArrayList<Customer> customerList = new ArrayList<>();
        String sql = """
                SELECT customer_id, first_name,
                last_name, country, postal_code,
                phone, email FROM customer OFFSET ? LIMIT ?""";

        ExecuteQuery(sql, preparedStatement -> {
            preparedStatement.setInt(1, offset);
            preparedStatement.setInt(2, limit);
        }, result -> {
            while (result.next()){
                customerList.add(readCustomer(result));
            }
        });
        return customerList;
    }

    @Override
    public CustomerCountry findCountryWithMostCustomers() {
        final CustomerCountry[] country = new CustomerCountry[1];
        String sql = """
                SELECT country
                FROM customer
                GROUP BY country
                HAVING COUNT (country) = (
                    SELECT MAX(country_count)
                    FROM
                    (
                        SELECT country, COUNT(country) country_count
                        FROM customer
                        GROUP BY country
                    )
                AS countries
                );""";

        ExecuteQuery(sql, result -> {
            if (result.next()) {
                country[0] = new CustomerCountry(result.getString("country"));
            }
        });
        return country[0];
    }

    @Override
    public CustomerSpender findHighestSpender(){
        final CustomerSpender[] customerSpender = new CustomerSpender[1];
        String sql = """
              SELECT  c.customer_id, c.first_name, c.last_name,
              c.country, c.postal_code, c.phone, c.email,  SUM(i.total) total_sum
              FROM invoice i
              JOIN customer c ON c.customer_id = i.customer_id
              GROUP BY c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email
              ORDER BY total_sum DESC
              LIMIT 1;""";

        ExecuteQuery(sql, result -> {
            if (result.next()) {
                customerSpender[0] = new CustomerSpender(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email"),
                    result.getDouble("total_sum")
                );
            }
        });
        return customerSpender[0];
    }

    @Override
    public ArrayList <CustomerGenre> findMostPopularGenreCustomer(int id) {
        final ArrayList <CustomerGenre> popularGenres = new ArrayList<>();
        String sql = """
                SELECT g.name
                FROM invoice i
                         JOIN customer c ON c.customer_id = i.customer_id
                         JOIN invoice_line il ON il.invoice_id = i.invoice_id
                         JOIN track t ON t.track_id = il.track_id
                         JOIN genre g ON g.genre_id = t.genre_id
                WHERE c.customer_id = ?
                GROUP BY g.name
                HAVING COUNT(t.genre_id) = (
                    SELECT MAX(genre_count) FROM (
                        SELECT COUNT(t.genre_id) genre_count
                         FROM invoice i
                                  JOIN customer c ON c.customer_id = i.customer_id
                                  JOIN invoice_line il ON il.invoice_id = i.invoice_id
                                  JOIN track t ON t.track_id = il.track_id
                                  JOIN genre g ON g.genre_id = t.genre_id
                         WHERE c.customer_id = ?
                         GROUP BY g.name
                         ) AS s_max_genre
                )""";

        ExecuteQuery(sql, preparedStatement -> {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, id);
        }, result -> {
            if (result.next()) {
                popularGenres.add(
                    new CustomerGenre(result.getString("name"))
                );
            }
        });
        return popularGenres;
    }
}