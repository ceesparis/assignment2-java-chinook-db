package nl.abc.chinookdbaccess.repositories;

import java.util.ArrayList;

public interface CRUDRepository <T, U>{

    /**
     * Method that finds all objects in the database.
     *
     * @return objectList {@link ArrayList<T>} which contains all objects
     */
    ArrayList<T> findAll();

    /**
     * Method that finds an object by id in the database.
     *
     * @return A single object {@link T}
     */
    T findById(U id);

    /**
     * Method that inserts a new object in the database.
     *
     * @return number of rows affected by the insert (1 in case method was successful)
     */
    int insert(T object);

    /**
     * Method that updates an existing object's values.
     *
     * @param object {@link T} the object that is to be updated
     * @return numbers of rows affected by the insert (1 in case method was successful)
     */
    int update(T object);

    /**
     * Method that deletes an object.
     *
     * @param object {@link T} object that is to be deleted
     * @return number of rows affect by the deleted (1 in case method was successful)
     */
    int delete(T object);

    /**
     * Method that deletes an object by its id.
     *
     * @param id {@link Integer} of the object that is to be deleted
     * @return number of rows affect by the deleted (1 in case method was successful)
     */
    int deleteById(U id);
}