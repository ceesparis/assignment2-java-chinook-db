package nl.abc.chinookdbaccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChinookDBAccess {

    public static void main(String[] args) {
        SpringApplication.run(ChinookDBAccess.class, args);
    }

}
