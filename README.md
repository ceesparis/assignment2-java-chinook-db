# Data access with JDBC

[![pipeline status](https://gitlab.com/ceesparis/assignment2-java-chinook-db/badges/main/pipeline.svg)](https://gitlab.com/ceesparis/assignment2-java-chinook-db/-/commits/main)

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
    <img src="spring.png" alt="Logo" width="80" height="80">


<h3 align="center">Data access with JDBC</h3>

  <p align="center">
    Create a database with postgresql and accessing a database with JDBC. 
  </p>
</div>



<!-- TABLE OF CONTENTS -->

  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#getting-started">Getting Started</a></li>
        <li><a href="#usage">Usage</a></li>
      </ul>
    </li>
    <li><a href="#functionality">Functionality</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>




<!-- ABOUT THE PROJECT -->
## About The Project

This is the second Java Assignment Project for the Noroff Java Bootcamp.
Where we were tasked with creating a superhero database using PostgreSQL and with accessing a database using JDBC with CRUD functionality.

Queries for the superhero database can be found in AppendixA/queries.

<img src="appendixA/ERD_Superhero.drawio.svg" alt="Logo" width="500" height="500">

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Java](https://www.java.com/nl/)

* [Spring](https://spring.io/)

* [Gradle](https://gradle.org/)

* [PostgreSQL](https://www.postgresql.org/)

* [Intellij](https://www.jetbrains.com/idea/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

```sh
git clone url
cd project-directory
./gradlew build
```


<!-- USAGE EXAMPLES -->
## Usage


```sh
./gradlew bootrun
``` 


<!-- Functionality EXAMPLES -->
## Functionality

<p>Customer functionality:</p>
<ul>
<li>Can return all the customers in the database.</li>
<li>Can return a specific customer from the database (by Id).</li>
<li>Can return a specific customer by name.</li>
<li>Can return a subset of customers.</li>
<li>Can add a new customer to the database.</li>
<li>Can update an existing customer.</li>
<li>Can return the country with the most customers.</li>
<li>Can return customer who is the highest spender.</li>
<li>Can return a given customers favorite genre.</li>


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- Maintainers -->
## Maintainers

<p>[@ceesparis](https://gitlab.com/ceesparis)</p>
<p>[@AxlBrancoDuarte](https://gitlab.com/AxlBrancoDuarte)</p>
<p>[@beau-c](https://gitlab.com/beau-c)</p>

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
